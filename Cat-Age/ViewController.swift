//
//  ViewController.swift
//  Cat-Age
//
//  Created by Ryan McHenry on 10/15/14.
//  Copyright (c) 2014 Develonaut. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
//  UI Variables
    @IBOutlet weak var promtLabel: UILabel!
    
    @IBOutlet weak var ageInput: UITextField!
    
    @IBOutlet weak var catAge: UILabel!
    
//  Submit Button For Cat Age
    @IBAction func submitButton(sender: AnyObject) {
    
        var age = ageInput.text.toInt()
        
        age = age! * 7
        catAge.text = "Your cat is \(age!) years old"
    
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

